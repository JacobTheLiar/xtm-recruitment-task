package pl.jacob_the_liar.xtm.wikipedia.configure;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 * * @author: Jakub O.  [https://github.com/JacobTheLiar]
 * * @date : 2020-10-18 21:48
 * *
 * * @className: WikipediaProperties
 * *
 * *
 ******************************************************/
@Data
@ConfigurationProperties(prefix = "wikipedia")
@Component
public class WikipediaProperties{
    
    private String baseUrl;
    private String queryUrl;
    private String extractUrl;
}
