package pl.jacob_the_liar.xtm.wikipedia.model;


import lombok.Data;

import java.util.Map;


/**
 * * @author: Jakub O.  [https://github.com/JacobTheLiar]
 * * @date : 2020-10-18 22:12
 * *
 * * @className: WikipediaUrlQuery
 * *
 * *
 ******************************************************/
@Data
public class WikipediaUrlQuery{
    
    private Map<String, WikipediaUrlPage> pages;
}
