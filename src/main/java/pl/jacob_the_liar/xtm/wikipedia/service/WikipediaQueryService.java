package pl.jacob_the_liar.xtm.wikipedia.service;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.jacob_the_liar.xtm.wikipedia.configure.WikipediaProperties;
import pl.jacob_the_liar.xtm.wikipedia.exception.InvalidResponseQueryException;
import pl.jacob_the_liar.xtm.wikipedia.exception.NoFootballClubFoundException;
import pl.jacob_the_liar.xtm.wikipedia.model.WikipediaQuery;
import pl.jacob_the_liar.xtm.wikipedia.model.WikipediaResponse;
import pl.jacob_the_liar.xtm.wikipedia.model.WikipediaSearch;

import java.util.Optional;


/**
 * * @author: Jakub O.  [https://github.com/JacobTheLiar]
 * * @date : 2020-10-18 22:50
 * *
 * * @className: WikipediaQueryService
 * *
 * *
 ******************************************************/
@Service
@RequiredArgsConstructor
public class WikipediaQueryService{
    
    private final WikipediaProperties wikipediaProperties;
    private final RestTemplate restTemplate;
    
    
    public WikipediaSearch getWikipediaSearchResult(String query){
        
        final WikipediaResponse response = restTemplate.getForObject(wikipediaProperties.getQueryUrl(),
                                                                     WikipediaResponse.class, query);
        
        if (isValidResponse(response)) {
            final Optional<WikipediaQuery> wikipediaQuery = Optional.ofNullable(response.getQuery());
            
            return wikipediaQuery.orElseThrow(InvalidResponseQueryException::new)
                    .getSearch()
                    .stream()
                    .filter(wikipediaSearch -> wikipediaSearch.getSnippet()
                            .toLowerCase()
                            .contains("football") && wikipediaSearch.getSnippet().toLowerCase().contains("club"))
                    .findFirst()
                    .orElseThrow(() -> new NoFootballClubFoundException(query));
        }
        
        throw new InvalidResponseQueryException();
    }
    
    
    private boolean isValidResponse(WikipediaResponse response){
        return response != null && response.getQuery() != null;
    }
}
