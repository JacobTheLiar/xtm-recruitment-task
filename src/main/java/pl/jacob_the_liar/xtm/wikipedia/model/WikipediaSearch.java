package pl.jacob_the_liar.xtm.wikipedia.model;


import lombok.Data;


/**
 * * @author: Jakub O.  [https://github.com/JacobTheLiar]
 * * @date : 2020-10-18 21:54
 * *
 * * @className: WikipediaSearch
 * *
 * *
 ******************************************************/
@Data
public class WikipediaSearch{
    
    private String title;
    private Long pageid;
    private String snippet;
}
