package pl.jacob_the_liar.xtm.wikipedia.service;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.jacob_the_liar.xtm.wikipedia.model.QueryResult;
import pl.jacob_the_liar.xtm.wikipedia.model.WikipediaSearch;


/**
 * * @author: Jakub O.  [https://github.com/JacobTheLiar]
 * * @date : 2020-10-18 20:43
 * *
 * * @className: WikipediaService
 * *
 * *
 ******************************************************/
@Service
@RequiredArgsConstructor
public class WikipediaService{
    
    private final WikipediaQueryService queryService;
    private final WikipediaUrlService urlService;
    
    
    public QueryResult findFootballClub(String query){
        
        final WikipediaSearch football = queryService.getWikipediaSearchResult(query);
        final String fullUrl = urlService.getWikipediaUrl(football.getPageid());
        
        return QueryResult.builder()
                .title(football.getTitle())
                .snippet(football.getSnippet())
                .directUrl(fullUrl)
                .build();
    }
}
