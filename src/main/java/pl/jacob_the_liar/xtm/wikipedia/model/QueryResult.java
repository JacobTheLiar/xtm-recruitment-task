package pl.jacob_the_liar.xtm.wikipedia.model;


import lombok.Builder;
import lombok.Data;


/**
 * * @author: Jakub O.  [https://github.com/JacobTheLiar]
 * * @date : 2020-10-18 20:50
 * *
 * * @className: QueryResult
 * *
 * *
 ******************************************************/
@Data
@Builder
public class QueryResult{
    
    private String title;
    private String snippet;
    private String directUrl;
}
