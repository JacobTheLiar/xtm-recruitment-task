package pl.jacob_the_liar.xtm.wikipedia.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


/**
 * * @author: Jakub O.  [https://github.com/JacobTheLiar]
 * * @date : 2020-10-18 23:04
 * *
 * * @className: InvalidResponseQueryException
 * *
 * *
 ******************************************************/
@ResponseStatus(HttpStatus.BAD_GATEWAY)
public class InvalidResponseQueryException extends RuntimeException{

}
