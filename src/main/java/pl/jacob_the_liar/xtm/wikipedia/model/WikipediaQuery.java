package pl.jacob_the_liar.xtm.wikipedia.model;


import lombok.Data;

import java.util.List;


/**
 * * @author: Jakub O.  [https://github.com/JacobTheLiar]
 * * @date : 2020-10-18 21:58
 * *
 * * @className: WikipediaQuery
 * *
 * *
 ******************************************************/
@Data
public class WikipediaQuery{
    
    List<WikipediaSearch> search;
}
