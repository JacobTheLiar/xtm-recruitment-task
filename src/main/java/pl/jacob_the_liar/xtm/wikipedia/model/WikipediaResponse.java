package pl.jacob_the_liar.xtm.wikipedia.model;


import lombok.Data;


/**
 * * @author: Jakub O.  [https://github.com/JacobTheLiar]
 * * @date : 2020-10-18 21:10
 * *
 * * @className: WikipediaResponse
 * *
 * *
 ******************************************************/
@Data
public class WikipediaResponse{
    
    private WikipediaQuery query;
}
