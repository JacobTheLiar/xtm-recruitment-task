package pl.jacob_the_liar.xtm.wikipedia.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


/**
 * * @author: Jakub O.  [https://github.com/JacobTheLiar]
 * * @date : 2020-10-18 22:06
 * *
 * * @className: NoFootballClubFoundException
 * *
 * *
 ******************************************************/
@ResponseStatus(HttpStatus.NOT_FOUND)
public class NoFootballClubFoundException extends RuntimeException{
    
    private static final String MESSAGE = "No Football Club found by query: %s";
    
    
    public NoFootballClubFoundException(String query){
        super(String.format(MESSAGE, query));
    }
}
