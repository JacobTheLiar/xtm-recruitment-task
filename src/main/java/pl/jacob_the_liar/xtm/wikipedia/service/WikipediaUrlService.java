package pl.jacob_the_liar.xtm.wikipedia.service;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.jacob_the_liar.xtm.wikipedia.configure.WikipediaProperties;
import pl.jacob_the_liar.xtm.wikipedia.exception.InvalidResponseQueryException;
import pl.jacob_the_liar.xtm.wikipedia.model.WikipediaUrlResponse;


/**
 * * @author: Jakub O.  [https://github.com/JacobTheLiar]
 * * @date : 2020-10-18 22:54
 * *
 * * @className: WikipediaUrlService
 * *
 * *
 ******************************************************/
@Service
@RequiredArgsConstructor
public class WikipediaUrlService{
    
    private final WikipediaProperties wikipediaProperties;
    private final RestTemplate restTemplate;
    
    
    public String getWikipediaUrl(Long pageId){
        final WikipediaUrlResponse response = restTemplate.getForObject(wikipediaProperties.getExtractUrl(),
                                                                        WikipediaUrlResponse.class, pageId.toString());
        
        if (isValidResponse(response)) {
            return response.getQuery().getPages().get(pageId.toString()).getFullurl();
        }
        
        throw new InvalidResponseQueryException();
    }
    
    
    private boolean isValidResponse(WikipediaUrlResponse response){
        return response != null && response.getQuery() != null && response.getQuery().getPages().size() > 0;
    }
}
