package pl.jacob_the_liar.xtm.wikipedia.utils;


import java.util.Scanner;


/**
 * * @author: Jakub O.  [https://github.com/JacobTheLiar]
 * * @date : 2020-10-19 11:50
 * *
 * * @className: Input
 * *
 * *
 ******************************************************/
public class Input{
    
    public static String getString(){
        Scanner scanner = new Scanner(System.in);
        while (true) {
            if (scanner.hasNextLine()) {
                return scanner.nextLine();
            } else {
                System.out.println("error, try again!");
            }
        }
    }
}
