package pl.jacob_the_liar.xtm.wikipedia;


import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.jacob_the_liar.xtm.wikipedia.exception.InvalidResponseQueryException;
import pl.jacob_the_liar.xtm.wikipedia.exception.NoFootballClubFoundException;
import pl.jacob_the_liar.xtm.wikipedia.model.QueryResult;
import pl.jacob_the_liar.xtm.wikipedia.service.WikipediaService;
import pl.jacob_the_liar.xtm.wikipedia.utils.Input;


@SpringBootApplication
@RequiredArgsConstructor
public class WikipediaApplication implements CommandLineRunner{
    
    private final WikipediaService wikipediaService;
    
    
    public static void main(String[] args){
        SpringApplication.run(WikipediaApplication.class, args);
    }
    
    
    @Override
    public void run(String... args) throws Exception{
        System.out.println("empty line will exit application");
        while (true) {
            System.out.println();
            System.out.print("Input: ");
            String input = Input.getString();
            System.out.print("Output: ");
            
            if (input.isEmpty()) break;
            
            try {
                final QueryResult result = wikipediaService.findFootballClub(input);
                System.out.println(result.getDirectUrl());
            } catch (NoFootballClubFoundException exception) {
                System.out.println("[no football club found]");
            } catch (InvalidResponseQueryException exception) {
                System.out.println("[invalid response from wikipedia]");
            } catch (Exception exception) {
                System.out.println("[unknown exception: " + exception.getMessage() + "]");
            }
        }
        System.out.print("[command line application ended]");
    }
}
