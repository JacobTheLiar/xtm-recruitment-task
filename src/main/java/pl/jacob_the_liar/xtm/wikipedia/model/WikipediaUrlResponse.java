package pl.jacob_the_liar.xtm.wikipedia.model;


import lombok.Data;


/**
 * * @author: Jakub O.  [https://github.com/JacobTheLiar]
 * * @date : 2020-10-18 22:11
 * *
 * * @className: WikipediaUrlResponse
 * *
 * *
 ******************************************************/
@Data
public class WikipediaUrlResponse{
    
    WikipediaUrlQuery query;
}
