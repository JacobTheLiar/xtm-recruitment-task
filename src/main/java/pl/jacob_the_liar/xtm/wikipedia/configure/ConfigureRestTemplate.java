package pl.jacob_the_liar.xtm.wikipedia.configure;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;


/**
 * * @author: Jakub O.  [https://github.com/JacobTheLiar]
 * * @date : 2020-10-18 21:03
 * *
 * * @className: ConfigureWebClient
 * *
 * *
 ******************************************************/
@Configuration
public class ConfigureRestTemplate{
    
    @Bean
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }
}
